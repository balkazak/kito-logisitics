$(function() {



	wow = new WOW(
		{
			boxClass:     'wow',      // default
			animateClass: 'animated', // default
			offset:       0,          // default
			mobile:       true,       // default
			live:         true        // default
		}
	);
	wow.init();

	$('.top-line-center span').on('click', function()  {
		$('.select-form').slideToggle('400');
		$('.top-line-center span .fa').toggleClass('arrows-active');
	});

	$('#phone-id').mask('+7(999) 999-99-99',{placeholder: "+7 (   )   -  -  "});
	

	//E-mail Ajax Send
	$("form").one('submit', function() { //Change
		var th = $(this);
		$.ajax({
			type: "POST",
			url: "mail.php", //Change
			data: th.serialize()
		}).done(function() {
			$(location).attr('href', '/thanks.html');

		});
		return false;
	});

	$('a[href*="#callback"], #modal').magnificPopup({
		type: 'inline',
		callbacks: {
			beforeOpen: function() {
				this.st.mainClass = this.st.el.attr('data-effect');
			}
		},
		midClick: true,
		closeBtnInside: true,
		preloader: false,

	});

});
var mySwiper = new Swiper ('.swiper-container', {
	// Optional parameters
	loop: true,
	slidesPerView: 1,
	spaceBetween: 500,
	// Navigation arrows
	navigation: {
		nextEl: '.next-1',
		prevEl: '.prev-1',
	},
	pagination: {
		el: '.swiper-pagination',
		type: 'bullets',
	},
});

